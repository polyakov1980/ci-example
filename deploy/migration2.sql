-- Deploy ci-example:migration2 to pg

BEGIN;

create table t1 as select i, random()::text as payload from generate_series(1, 100000000) i;

-- now expensive DDL -- should be a full table scan sitting in exclude table-level lock;
-- the test should work in all Postgres versions but let's check (maybe you'll need to decrease N)
alter table t1 alter column i set not null;


COMMIT;
